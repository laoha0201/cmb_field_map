# CMB Field Type: Baidu Maps
## 说明
此为老蟹改编为百度地图，改为百度地图后添加了街景选择，不带街景时用
```php
array(
	'name' => '位置',
	'desc' => '请拖动图标或在任意位置双击来改变位置',
	'id' => $prefix . 'location',
	'type' => 'pw_map',
	'sanitization_cb' => 'pw_map_sanitise',
),
```
带街景时用：
```php
array(
	'name' => '位置',
	'desc' => '请拖动图标或在任意位置双击来改变位置，可在街景图上微调方向或角度',
	'id' => $prefix . 'location',
	'type' => 'pw_panmap',
	'sanitization_cb' => 'pw_panmap_sanitise',
),
```

##此改编于: https://github.com/mustardBees/cmb_field_map 原为google地图,说明见下:

## Description

Google Maps field type for [CMB2](https://github.com/WebDevStudios/CMB2 "Custom Metaboxes and Fields for WordPress 2").

**Running an older version of CMB? Check the [previous releases](https://github.com/mustardBees/cmb_field_map/releases).**

The `pw_map` field stores the latitude/longitude values which you can then use to display a map in your theme.

## Installation

You can install this field type as you would a WordPress plugin:

1. Download the plugin
2. Place the plugin folder in your `/wp-content/plugins/` directory
3. Activate the plugin in the Plugin dashboard

Alternatively, you can place the plugin folder in with your theme/plugin. After you call CMB:

```php
require_once 'init.php';
```

Add another line to include the `cmb-field-map.php` file. Something like:

```php
require_once 'cmb_field_map/cmb-field-map.php';
```

## Usage

`pw_map` - Save a location on a map. Example:

```php
array(
	'name' => 'Location',
	'desc' => 'Drag the marker to set the exact location',
	'id' => $prefix . 'location',
	'type' => 'pw_map',
	'sanitization_cb' => 'pw_map_sanitise',
),
```

## Screenshot

![Image](screenshot-1.png?raw=true)
