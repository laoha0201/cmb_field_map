<?php
/*
Plugin Name: CMB Field Type: Baidu Maps
Plugin URI: https://git.oschina.net/laoha0201/cmb_field_map
Description: Baidu Maps field type for Custom Metaboxes and Fields for WordPress.
Version: 1.0.0
Author: 老蟹
Author URI: https://git.oschina.net/laoha0201
License: GPLv2+
*/
// 此改编于: https://github.com/mustardBees/cmb_field_map 原为google地图
// Useful global constants
define( 'PW_BAIDU_MAPS_URL', plugin_dir_url( __FILE__ ).'/cmb-baidu-map/' );

/**
 * Render field
 */
function pw_map_field( $field, $meta ) {
    wp_enqueue_script( 'pw_baidu_maps_api', 'http://api.map.baidu.com/api?v=2.0&ak=vdI972BlO8nqKIoyZ7YOZEy5', array(), null );
	wp_enqueue_script( 'pw_baidu_maps_init', PW_BAIDU_MAPS_URL . 'js/script.js', array( 'pw_baidu_maps_api' ), null );
	wp_enqueue_style( 'pw_baidu_maps_css', PW_BAIDU_MAPS_URL . 'css/style.css', array(), null );

	echo '<div class="map" id="baidumap"></div>';
	echo '<input type="hidden" id="latitude" name="' . $field->args( 'id' ) . '[lat]" value="' . ( isset( $meta['lat'] ) ? $meta['lat'] : '' ) . '" />';
	echo '<input type="hidden" id="longitude" name="' . $field->args( 'id' ) . '[lng]" value="' . ( isset( $meta['lng'] ) ? $meta['lng'] : '' ) . '" />';

	$desc = $field->args( 'desc' );
	if ( ! empty( $desc ) ) echo '<p class="cmb2-metabox-description">' . $desc . '</p>';
}
add_filter( 'cmb2_render_pw_map', 'pw_map_field', 10, 2 );

/**
 * Split latitude/longitude values into two meta fields
 */
function pw_map_sanitise( $meta_value, $field ) {
	$lat = $meta_value['lat'];
	$lng = $meta_value['lng'];
	$panoid = $meta_value['panoid'];
	$heading = $meta_value['heading'];
	$pitch = $meta_value['pitch'];
	if ( ! empty( $lat ) ) {
		update_post_meta( get_the_ID(), $field['id'] . '_lat', $lat );
	}

	if ( ! empty( $lng ) ) {
		update_post_meta( get_the_ID(), $field['id'] . '_lng', $lng );
	}
	return $meta_value;
}

//带街景
function pw_panmap_field( $field, $meta ) {
	wp_enqueue_script( 'pw_baidu_maps_api', 'http://api.map.baidu.com/api?v=2.0&ak=vdI972BlO8nqKIoyZ7YOZEy5', array(), null );
	wp_enqueue_script( 'pw_baidu_maps_init', PW_BAIDU_MAPS_URL . 'js/panscript.js', array( 'pw_baidu_maps_api' ), null );
	wp_enqueue_style( 'pw_baidu_maps_css', PW_BAIDU_MAPS_URL . 'css/style.css', array(), null );

	echo '<div class="map" id="mapsPano"></div>';
	echo '<div class="map" id="baidumap"></div>';
	echo '<input type="hidden" id="latitude" name="' . $field->args( 'id' ) . '[lat]" value="' . ( isset( $meta['lat'] ) ? $meta['lat'] : '' ) . '" />';
	echo '<input type="hidden" id="longitude" name="' . $field->args( 'id' ) . '[lng]" value="' . ( isset( $meta['lng'] ) ? $meta['lng'] : '' ) . '" />';
	echo '<input type="hidden" id="inputPano" name="' . $field->args( 'id' ) . '[panoid]" value="' . ( isset( $meta['panoid'] ) ? $meta['panoid'] : '' ) . '" />';
	echo '<input type="hidden" id="inputPanoH" name="' . $field->args( 'id' ) . '[heading]" value="' . ( isset( $meta['heading'] ) ? $meta['heading'] : '' ) . '" />';
	echo '<input type="hidden" id="inputPanoP" name="' . $field->args( 'id' ) . '[pitch]" value="' . ( isset( $meta['pitch'] ) ? $meta['pitch'] : '' ) . '" />';

	$desc = $field->args( 'desc' );
	if ( ! empty( $desc ) ) echo '<p class="cmb2-metabox-description">' . $desc . '</p>';
}
add_filter( 'cmb2_render_pw_panmap', 'pw_panmap_field', 10, 2 );

/**
 * Split latitude/longitude values into two meta fields
 */
function pw_panmap_sanitise( $meta_value, $field ) {
	$lat = $meta_value['lat'];
	$lng = $meta_value['lng'];
	$panoid = $meta_value['panoid'];
	$heading = $meta_value['heading'];
	$pitch = $meta_value['pitch'];
	if ( ! empty( $lat ) ) {
		update_post_meta( get_the_ID(), $field['id'] . '_lat', $lat );
	}

	if ( ! empty( $lng ) ) {
		update_post_meta( get_the_ID(), $field['id'] . '_lng', $lng );
	}

	if ( ! empty( $panoid ) ) {
		update_post_meta( get_the_ID(), $field['id'] . '_panoid', $panoid );
	}
	if ( ! empty( $heading ) ) {
		update_post_meta( get_the_ID(), $field['id'] . '_heading', $heading );
	}
	if ( ! empty( $pitch ) ) {
		update_post_meta( get_the_ID(), $field['id'] . '_pitch', $pitch );
	}
	return $meta_value;
}
