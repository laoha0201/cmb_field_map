/**
 * yinyuetai wp_embed handler
 *
 * laoha add 音悦台
 *	http://v.yinyuetai.com/video/2173703 http://player.yinyuetai.com/video/swf/2866434/v_0.swf
 *	http://v.yinyuetai.com/playlist/2173703 http://player.yinyuetai.com/playlist/swf/2866434/v_0.swf
 * @since 3.4.0
 */
function wp_embed_handler_yinyuetai( $matches, $attr, $url, $rawattr ) {
	$embed = sprintf(
		'<embed src="http://player.yinyuetai.com/%1$s/swf/%2$s/v_0.swf" allowFullScreen="true" quality="high" width="480" height="350" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>',$matches['video_type'],
		esc_attr( $matches['video_id'] ) );

	return apply_filters( 'embed_yinyuetai', $embed, $matches, $attr, $url, $rawattr );
}
wp_embed_register_handler( 'yinyuetai',
	'#https?://v\.yinyuetai\.com/(?<video_type>video|playlist)/(?<video_id>[0-9]+)#i',
	'wp_embed_handler_yinyuetai' );
