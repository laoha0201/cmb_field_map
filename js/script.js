(function ($) {
		var map = new BMap.Map("baidumap");
		var lat = $('#latitude');
		var lng = $('#longitude');		
		var point = new BMap.Point(120.987998,31.389297);
		var zoom = 14;

		// Map
		if(lat.val().length > 0 && lng.val().length > 0) {
			point = new BMap.Point(lng.val(),lat.val());
			zoom = 16;
		}

		
		map.centerAndZoom(point,zoom);
		map.disableDoubleClickZoom();
		map.addControl(new BMap.NavigationControl());    //控制地图的平移和缩放的功能
		map.addControl(new BMap.MapTypeControl());    //添加地图类型控件
		map.setMaxZoom(18);
		map.setMinZoom(12);
	
		var marker=new BMap.Marker(point);
		marker.setTitle('拖动图标改变位置设定') 
		marker.enableDragging();
		map.addOverlay(marker);  
		marker.addEventListener('dragend',function(e){
			map.panTo(e.point);
			lat.val(e.point.lat);
			lng.val(e.point.lng);	
		});		
		map.addEventListener('dblclick',function(e){
			map.panTo(e.point);
			marker.setPosition(e.point) 
			lat.val(e.point.lat);
			lng.val(e.point.lng);	
		});			

}(jQuery));
