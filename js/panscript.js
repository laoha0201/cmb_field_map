(function ($) {
		var map = new BMap.Map("baidumap");
		var lat = $('#latitude');
		var lng = $('#longitude');		
		var point = new BMap.Point(120.987998,31.389297);
		var	panoId= "01002900001405311459453805W" ;
		var	panoH = 4 ;
		var	panoP = -6 ;
		var zoom = 14;



		// Map
		if(lat.val().length > 0 && lng.val().length > 0) {
			point = new BMap.Point(lng.val(),lat.val());
			zoom = 16;
			panoId = $('#inputPano').val() ;
			if($('#inputPano').val()!=''){
				panoH = $('#inputPanoH').val();
			}
			if($('#inputPano').val()!=''){
				panoP = $('#inputPanoP').val();
			}
		}

		map.centerAndZoom(point,zoom);
		map.disableDoubleClickZoom();
		map.addControl(new BMap.NavigationControl());    //控制地图的平移和缩放的功能
		map.addControl(new BMap.MapTypeControl());    //添加地图类型控件
		map.setMaxZoom(18);
		map.setMinZoom(12);


		var panorama = new BMap.Panorama('mapsPano');
		panorama.disableScrollWheelZoom() ;
		if(panoId!=''){
			panorama.setId(panoId);
		}else{
			panorama.setPosition(point); //根据经纬度坐标展示全景图
		}
		panorama.setPov({heading: panoH, pitch: panoP});



		panorama.addEventListener('position_changed', function(e){ //全景图位置改变后，普通地图中心点也随之改变
				$("#inputPano").val(panorama.getId());
				var pov = panorama.getPov();
				if(pov.heading>180){
					a = pov.heading-1;
				}else{
					a = pov.heading+1;
				}
				panorama.setPov({heading:a, pitch: pov.pitch}); //解决改变位置后角度不变的BUG
			});
		panorama.addEventListener('pov_changed', function(e){ //全景视角发生变化时触发
				var pov = panorama.getPov();
				$("#inputPanoH").val(pov.heading);
				$("#inputPanoP").val(pov.pitch);
			});

 
		var panoramaService = new BMap.PanoramaService();	

		var marker=new BMap.Marker(point);
		marker.setTitle('拖动图标改变位置设定') ;
		marker.enableDragging();
		map.addOverlay(marker);  

		marker.addEventListener('dragend',function(e){
			map.panTo(e.point);
			lat.val(e.point.lat);
			lng.val(e.point.lng);			
			panoramaService.getPanoramaByLocation(e.point,500,function(data){
					if (data == null) {
							console.log('no data');
							return;
					}else{
						panorama.setId(data.id);
					}
			});
		});		
		map.addEventListener('dblclick',function(e){
			map.panTo(e.point);
			marker.setPosition(e.point) 
			lat.val(e.point.lat);
			lng.val(e.point.lng);	
			panoramaService.getPanoramaByLocation(e.point,500,function(data){
					if (data == null) {
							console.log('no data');
							return;
					}else{
						panorama.setId(data.id);
					}
			});
		});		

		
}(jQuery));
